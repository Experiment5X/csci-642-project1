import java.math.BigInteger;
import edu.rit.cs.MyTimer;

/**
 * Code to handle parsing command line arguments and actually running the
 * Lemoine conjecture code.
 */
public class Lemoine {

    /**
     * Test the lemoine code
     */
    public static void testLemoineConjecture() {
        PrimeIteratorFactory iterFactory = new PrimeIteratorFactory(1746551);

        LemoineSolution solution1 = LemoineConjecture.findSolutionBest(7, iterFactory);
        assert solution1.getPrime() == 3 && solution1.getTwicePrime() == 2;

        LemoineSolution solution2 = LemoineConjecture.findSolutionBest(13, iterFactory);
        assert solution2.getPrime() == 3 && solution2.getTwicePrime() == 5;

        LemoineSolution solution3 = LemoineConjecture.findSolutionBest(47, iterFactory);
        assert solution3.getPrime() == 13 && solution3.getTwicePrime() == 17;

        LemoineSolution solution4 = LemoineConjecture.findSolutionBest(1746551, iterFactory);
        assert solution4.getPrime() == 1237 && solution4.getTwicePrime() == 872657;
    }

    /**
     * Wrapper for finding a Lemoine solution for a specified number. It will also
     * update the largest solution found (smallest P).
     */
    private static void findLemoineSolution(int curNum, PrimeIteratorFactory iterFactory, 
                                            LemoineSolution[] largestSolution) {
        LemoineSolution solution = LemoineConjecture.findSolutionBetter(curNum);

        synchronized(largestSolution) {
            if (largestSolution[0] == null ||
                solution.getPrime() > largestSolution[0].getPrime() ||
                (solution.getPrime() == largestSolution[0].getPrime() &&
                solution.getTwicePrime() > largestSolution[0].getTwicePrime())
            ) {
                largestSolution[0] = solution;
            }
        }
    }

    /**
     * Find a lemoine solution for a range of numbers with the smallest P.
     * 
     * @param parallel If true, all cores will be used for finding the Lemoine solution
     * @param args The command line arguments to the program
     */
    public static void findSolution(boolean parallel, String[] args) {
        //testLemoineConjecture();

        if (args.length != 2) {
            System.out.println("Usage: java com.adamspindler.Main MIN MAX");
            return;
        }

        int min = Integer.parseInt(args[0]);
        int max = Integer.parseInt(args[1]);

        if (min < 7) {
            System.out.println("The minimum value that the conjecture works for is 7: Min must be greater than 7.");
            return;
        }

        if (min >= max) {
            System.out.println("Min must be less than max.");
            return;
        }

        // the conjecture is only for odd numbers and we're skipping by 2 so we need to start
        // at an odd number
        if (min % 2 == 0) {
            min++;
        }

        MyTimer timer = new MyTimer("Timer");
        timer.start_timer();

        LemoineSolution[] largestSolution = new LemoineSolution[1];
        PrimeIteratorFactory iterFactory = new PrimeIteratorFactory(max);

        if (parallel) {
            // omp parallel for
            for (int curNum = min; curNum < max; curNum += 2) {
                findLemoineSolution(curNum, iterFactory, largestSolution);
            }
        } else {
            for (int curNum = min; curNum < max; curNum += 2) {
                findLemoineSolution(curNum, iterFactory, largestSolution);
            }
        }

		System.out.printf("%s\n", largestSolution[0]);

        timer.stop_timer();
        //timer.print_elapsed_time();
    }
}
