/**
 * Wrapper program for running the algorithm sequentially
 */
public class LemoineSeq {
    public static void main(String[] args) {
        Lemoine.findSolution(false, args);
    }
}