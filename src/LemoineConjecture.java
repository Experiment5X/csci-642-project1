import java.math.BigInteger;
import edu.rit.cs.Prime;

/**
 * Do all of the work to find a Lemoine solution for a given odd number
 */
public class LemoineConjecture {
    /** 
     * Calculate the basic lemoine equation
     * @param p Prime P
     * @param q Prime Q
     *
     * @return The lemoine equation value
     */
    private static int lemoine(int p, int q) {
        return p + 2 * q;
    }

    /**
     * Find the lemoine solution with the largest P value. This is the fastest solution.
     *
     * @param number The number for which to find the solution
     * @param iterFactory The factory for creating prime number iterators
     */
    public static LemoineSolution findSolutionBest(int number, PrimeIteratorFactory iterFactory) {
        PrimeArrayIterator primeIterator = iterFactory.getIterator();
        PrimeArrayIterator twiceIterator = iterFactory.getIterator();
        twiceIterator.seekToPrimeLessThan((number - 2) / 2);
        twiceIterator.next();

        int p = primeIterator.next();
        int q = twiceIterator.prev();

        while (twiceIterator.hasPrev()) {
            while (lemoine(p, q) > number && twiceIterator.hasPrev()) {
                q = twiceIterator.prev();
            }

            if (lemoine(p, q) == number) {
                return new LemoineSolution(number, p, q);
            }

            p = primeIterator.next();

            if (lemoine(p, q) == number) {
                return new LemoineSolution(number, p, q);
            }
        }

        return null;
    }

    /**
     * Find the lemoine solution with the largest P value. This is the middle fastest solution.
     *
     * @param number The number for which to find the solution
     */
    public static LemoineSolution findSolutionBetter(int number) {
        Prime.Iterator primeIterator = new Prime.Iterator(2);
        Prime.Iterator twiceIterator = new Prime.Iterator((number - 2) / 2);
        twiceIterator.next();

        int p = primeIterator.next();
        int q = twiceIterator.next();

        while (twiceIterator.hasPrev()) {
            while (lemoine(p, q) > number) {
                q = twiceIterator.prev();
            }

            if (lemoine(p, q) == number) {
                return new LemoineSolution(number, p, q);
            }

            p = primeIterator.next();

            if (lemoine(p, q) == number) {
                return new LemoineSolution(number, p, q);
            }
        }

        return null;
    }

    /**
     * Find the lemoine solution with the largest P value. This is the slowest solution.
     *
     * @param number The number for which to find the solution
     */
    public static LemoineSolution findSolutionWorst(int number) {
        Prime.Iterator primeRangeIterator = new Prime.Iterator();
        Prime.Iterator twiceRangeIterator = new Prime.Iterator();

        while (true) {
            int prime = primeRangeIterator.next();
            int twice = twiceRangeIterator.next();

            Prime.Iterator primeIterator = new Prime.Iterator();
            Prime.Iterator twiceIterator = new Prime.Iterator();

            // try all the primeValues up to this twiceValue
            int primeIterVal = 2;
            while (primeIterVal <= prime) {
                LemoineSolution potentialSolution = new LemoineSolution(number, primeIterVal, twice);
                if (potentialSolution.isValid()) {
                    return potentialSolution;
                }
                primeIterVal = primeIterator.next();
            }

            // try all the twiceValues up to this primeValue
            int twiceIterVal = 2;
            while (twiceIterVal < twice) {
                LemoineSolution potentialSolution = new LemoineSolution(number, prime, twiceIterVal);
                if (potentialSolution.isValid()) {
                    return potentialSolution;
                }
                twiceIterVal = twiceIterator.next();
            }
        }
    }
}
