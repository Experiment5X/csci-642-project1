import edu.rit.cs.Prime;
import java.util.ArrayList;

/**
 * Factory for creating iterators that use the shared pre-calculated array of primes
 */
public class PrimeIteratorFactory {
    private Integer[] primes;

    /**
     * Create a new factory and generate all the primes less than maxNum
     *
     * @param maxNum the max prime value to be calculated
     */
    public PrimeIteratorFactory(int maxNum) {
        ArrayList<Integer> primesList = new ArrayList<>();

        // generate primes up to a certain value
        Prime.Iterator iter = new Prime.Iterator();
        int curPrime = 2;
        do {
            primesList.add(curPrime);
            curPrime = iter.next();

        } while (curPrime < maxNum);

        this.primes = new Integer[primesList.size()];
        primesList.toArray(this.primes);
    }

    /**
     * @return the iterator for all the primes that were already calculated
     */
    public PrimeArrayIterator getIterator() {
        return new PrimeArrayIterator(this.primes);
    }
}
