import java.math.BigInteger;

/**
 * Class for encapsulating a Lemoine solution
 */
public class LemoineSolution {
    private int number;
    private int prime;
    private int twicePrime;

    /**
     * Create a new LemoineSolution object
     * 
     * @param number The number for which p and q satisfy the equation
     * @param prime P value
     * @param twicePrime Q value
     */
    public LemoineSolution(int number, int prime, int twicePrime) {
        this.number = number;
        this.prime = prime;
        this.twicePrime = twicePrime;
    }

    /**
     * @param True if p and q yield the num, otherwise false
     */
    public boolean isValid() {
        int rightHandSide = prime + 2 * twicePrime;
        return this.number == rightHandSide;
    }

    @Override
    public String toString() {
       return String.format("%s = %s + 2 * %s", number, prime, twicePrime);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getPrime() {
        return prime;
    }

    public void setPrime(int prime) {
        this.prime = prime;
    }

    public int getTwicePrime() {
        return twicePrime;
    }

    public void setTwicePrime(int twicePrime) {
        this.twicePrime = twicePrime;
    }
}
