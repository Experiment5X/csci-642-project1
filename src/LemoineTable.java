import edu.rit.cs.Prime;

/**
 * Program to help visualize the Lemoine solution, not part of the actual solution.
 */
public class LemoineTable {
    private static String getPaddedString(int totalChars, int val) {
        String valStr = Integer.toString(val);
        int spacesToAdd = totalChars - valStr.length();
        for (int i = 0; i < spacesToAdd; i++) {
            valStr = " " + valStr;
        }

        return valStr;
    }

    public static void main(String[] args) {
        int size = Integer.parseInt(args[0]);

        int threshold = -1;
        if (args.length == 2) {
            threshold = Integer.parseInt(args[1]);
        }

        int totalSpaces = 5;

        Prime.Iterator qIterator = new Prime.Iterator();
        for (int qIndex = 0; qIndex < size; qIndex++) {
            int q;
            if (qIndex == 0) {
                q = 2;

                System.out.print("#\t");
                Prime.Iterator displayIterator = new Prime.Iterator();
                for (int i = 0; i < size; i++) {
                    int val = (i == 0) ? 2 : displayIterator.next();
                    System.out.print(getPaddedString(totalSpaces, val));
                }
                System.out.print("\n\n");
            }
            else {
                q = qIterator.next();
            }

            Prime.Iterator pIterator = new Prime.Iterator();
            for (int pIndex = 0; pIndex < size; pIndex++) {
                
                int p;
                if (pIndex == 0) {
                    p = 2;
                    System.out.printf("%d\t", q);
                }
                else {
                    p = pIterator.next();
                }

                int lemoineVal = p + 2 * q;
                if (threshold != -1) {
                    lemoineVal = (lemoineVal > threshold) ? 1 : 0;
                }

                String lemoineValStr = getPaddedString(totalSpaces, lemoineVal);
                System.out.print(lemoineValStr);
            }

            System.out.println();
        }
    }
}