import java.util.Iterator;


/**
 Prime iterator for a pre-calculated list of prime numbers
 */
public class PrimeArrayIterator {
    private Integer[] primes;
    private int index = 0;

    /**
     * Create a new PrimeArrayIterator from the array of primes already calculated
     *
     * @param primes The pre-calculated array of primes
     */
    public PrimeArrayIterator(Integer[] primes) {
        this.primes = primes;
    }

    /**
     * Seek the iterator to the last prime that is less than the number
     *
     * @param num The number to seek near
     */
    public void seekToPrimeLessThan(int num) {
        int a = 0;
        int b = primes.length - 1;
        while (a < b) {
            int middle = (a + b) / 2;
            if (primes[middle] < num) {
                a = middle + 1;
            } else if (primes[middle] > num) {
                b = middle;
            } else {
                break;
            }
        }
        this.index = b - 1;
    }


    /**
     * @return true if there's a next prime, otherwise false
     */
    public boolean hasNext() {
        return index < primes.length;
    }

    /**
     * @return The next number in the primes array
     */
    public int next() {
        int prime = this.primes[this.index];
        this.index++;

        return prime;
    }

    /**
     * @return True if there is another prime before the current one, otherwise false
     */
    public boolean hasPrev() {
        return this.index >= 0;
    }

    /**
     * @return The previous prime number
     */
    public int prev() {
        int prime = this.primes[this.index];
        this.index--;

        return prime;
    }
}
