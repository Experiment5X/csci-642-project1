/**
 * Wrapper program for running the algorithm in parallel
 */
public class LemoineParallel {
    public static void main(String[] args) {
        Lemoine.findSolution(true, args);
    }
}