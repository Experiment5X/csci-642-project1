- Iterator needs to start at 0 so that you get 2 as a prime number
- The smallest n for the conjecture is 7, it says on wikipedia: 
    ```
    2n + 1 = p + q, for n > 2
    ```
  So for `n = 3`, it would be `7`.
  