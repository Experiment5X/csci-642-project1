# Lemoine Conjecture Solver

This program will find a Lemoine equation with the largest P value for a range of numbers.

## Running
Edit the Makefile to change the `OMP4J_PATH` variable to match the location of the JAR on your machine.
```
make
java -cp out LemoineSeq 1000000 1999999
java -cp out LemoineParallel 1000000 1999999
```
