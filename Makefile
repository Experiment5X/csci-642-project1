OMP4J_PATH = ~/Developer/ParallelSystems/lib/omp4j-1.2.jar

all:
	@java -jar $(OMP4J_PATH) -d out ./src/Lemoine* ./src/Prime* ./src/edu/rit/cs/*

compile-test:
	@javac -d out2 ./src/Lemoine* ./src/Prime* ./src/edu/rit/cs/*

run:
	@cd ./out && java -cp out LemoineSeq

pre:
	@rm -rf ./preprocessed
	@mkdir ./preprocessed
	@cp -R ./src/* ./preprocessed/
	@java -jar $(OMP4J_PATH) --no-compile -d preprocessed ./src/Lemoine* ./src/Prime* ./src/edu/rit/cs/*

install:
	@scp -r out/ axs9701@kraken.cs.rit.edu:/home/stu1/s17/axs9701/Developer/ParallelSystems/Project1/

clean:
	@rm -rf out preprocessed

INTELLIJ_PROJ_PATH = /Users/adamspindler/Developer/ParallelSystems/Project1/src/
intellij: pre
	@rm -rf $(INTELLIJ_PROJ_PATH)
	@mkdir -p $(INTELLIJ_PROJ_PATH)
	@cp -R ./preprocessed/ $(INTELLIJ_PROJ_PATH)

